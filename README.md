# Angular Grid Integration App #
## 1.0 ##

This app integrates Angular UI with backend and shows large amount of data into grid using [ui-grid](http://ui-grid.info/). It does have lots of features to be used with grid.


## Requirements ##

* Java 8 - https://java.com/en/download/
* Maven - https://maven.apache.org/download.cgi
* MySQL - https://dev.mysql.com/downloads/installer/
* Node.js - https://nodejs.org/en/download/

## MySQL Setup ##

Create user and grant permissions -

```
#!sql

CREATE DATABASE grid_integration;

CREATE USER 'grid'@'localhost' IDENTIFIED BY 'grid@123';

GRANT ALL ON grid_integration.* TO 'grid'@'localhost';

SHOW GRANTS FOR 'grid'@'localhost';
```

## How to run? ##

**Run API -**

  ```mvn spring-boot:run```

Add some test data in ```users``` table which will be created once the API is up and running.

**Run UI -**
 
  * Install node dependencies - ```npm install```
  * Install bower dependencies - ```bower install```
  * Run App - ```gulp```
    * If you get gulp not found error then run following command - ```npm install --global gulp-cli```