// Karma configuration
// Generated on Fri Jul 01 2016 17:57:11 GMT+0300 (EEST)

module.exports = function (config) {
  config.set({

    basePath  : '.',
    frameworks: ['jasmine'],

    files: [
      'bower_components/angular/angular.min.js',
      'bower_components/jquery/dist/jquery.min.js',
      'bower_components/angular-translate/angular-translate.min.js',
      'bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js',
      'bower_components/angular-translate-storage-local/angular-translate-storage-local.min.js',
      'bower_components/lodash/dist/lodash.min.js',
      'bower_components/angular-animate/angular-animate.min.js',
      'bower_components/angular-bootstrap/ui-bootstrap.min.js',
      'bower_components/angular-ui-router/release/angular-ui-router.min.js',
      'bower_components/angular-mocks/angular-mocks.js',
      'app/index.module.js',
      'app/index.config.js',
      'app/index.routes.js',
      'app/index.run.js',
      'app/src/**/*.tpl.html',
      'app/src/**/*.js'
    ],

    exclude: [],

    plugins: [
      'karma-jasmine',
      'karma-chrome-launcher',
      'karma-phantomjs-launcher',
      'karma-coverage',
      'karma-ng-html2js-preprocessor'
    ],

    preprocessors: {
      '**/*.tpl.html': ['ng-html2js']
    },

    reporters: ['progress'],

    port: 9876,

    colors: true,

    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    autoWatch: true,

    browsers: ['PhantomJS'],

    //avoid DISCONNECT test failures because of slow execution
    browserNoActivityTimeout: 60000,

    singleRun: false,

    concurrency: Infinity,

    ngHtml2JsPreprocessor: {
      stripPrefix: '.*app/',
      moduleName : 'karmaTemplates'
    }
  })
};
