module.exports = function (gulp, $, config) {
  return function () {
    console.log('in app-config task');
    var backendUrl = config.backendProtocol + '://' + config.backendHost + ':' + config.backendPort;
    return $.ngConstant({
        name     : 'angularGridApp',
        deps     : false,
        constants: {
          appConfig: {
            apiEndpointUrl   : backendUrl,
            apiBase          : config.apiBase
          }
        },
        stream   : true
      })
      .pipe(gulp.dest(config.path.build + 'config/'));
  };

};
