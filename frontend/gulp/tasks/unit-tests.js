'use strict';

var karma      = require('karma');
module.exports = function (gulp, $, conf, path) {

  var pathSrcHtml = [
    path.join(conf.path.src, '**/*.tpl.html')
  ];

  var pathSrcJs = [
    path.join(conf.path.src, '**/!(*.spec).js')
  ];

  function runTests(singleRun, done) {
    var reporters     = ['progress'];
    var preprocessors = {};

    pathSrcHtml.forEach(function (path) {
      preprocessors[path] = ['ng-html2js'];
    });

    if (singleRun) {
      pathSrcJs.forEach(function (path) {
        preprocessors[path] = ['coverage'];
      });
      reporters.push('coverage')
    }

    var localConfig = {
      configFile   : path.join(__dirname, '/../../karma.conf.js'),
      singleRun    : singleRun,
      autoWatch    : !singleRun,
      reporters    : reporters,
      preprocessors: preprocessors
    };

    var server = new karma.Server(localConfig, function (failCount) {
      done(failCount ? new Error("Failed " + failCount + " tests.") : null);
    });

    server.start();
  }

  return runTests;
};
