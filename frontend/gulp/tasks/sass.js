/**
 * This task is used to compile sass files
 */
'use strict';
require('es6-promise').polyfill();
var autoprefixer = require('autoprefixer');
var csso         = require('postcss-csso');

module.exports = function (gulp, $, config, path, cachebust) {
  return function () {
    console.log('Compiling sass to css');

    return gulp.src(path.join(config.path.app, '**/*.scss'))
      .pipe($.plumber())
      .pipe($.environments.development($.sourcemaps.init()))
      .pipe($.sass())
      .pipe($.environments.production(gulp.dest(path.join(config.path.build, 'assets/styles/debug/'))))
      .pipe($.postcss([
        autoprefixer({ browsers: config.supportedBrowsers }),
        csso({ restructure: $.environments.production() })
      ]))
      .pipe($.environments.development($.sourcemaps.write()))
      .pipe($.environments.production(cachebust.resources()))
      .pipe($.size())
      .pipe(gulp.dest(path.join(config.path.build, 'assets/styles')))
      .pipe($.connect.reload());
  };
};