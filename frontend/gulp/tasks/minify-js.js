var mainBowerFiles = require('main-bower-files');
var merge          = require('merge-stream');
module.exports = function (gulp, $, config, path, cachebust) {
  return function () {
    console.log('minifying js');
    var jsFilter   = $.filter('**/*.js', '!**/*.min.js');
    var production = $.environments.production;

    var main = gulp.src(
      [
        path.join(config.path.app, 'index.module.js'),
        path.join(config.path.build, 'config/*.js'),
        path.join(config.path.app, 'index.config.js'),
        path.join(config.path.app, 'index.routes.js'),
        path.join(config.path.app, 'index.run.js'),
        path.join(config.path.src, '**/*.js'),
        path.join(config.path.build, 'temp/**/*.js'),
        '!' + path.join(config.path.app, '**/*.spec.js')
      ]
    )
      .pipe($.plumber())
      .pipe($.eslint())
      .pipe($.eslint.format())
      .pipe($.ngAnnotate())
      .pipe($.concat('main.js'))
      .pipe(production(gulp.dest(path.join(config.path.build, 'scripts/debug/'))))
      .pipe(production($.uglify()))
      .pipe(production(cachebust.resources()))
      .pipe($.size())
      .pipe(gulp.dest(path.join(config.path.build, 'scripts')))
      .pipe($.connect.reload());

    var vendor = gulp.src(mainBowerFiles())
      .pipe(jsFilter)
      .pipe($.concat('vendor.js'))
      .pipe(production(gulp.dest(path.join(config.path.build, 'scripts/debug/'))))
      .pipe(production($.uglify()))
      .pipe(production(cachebust.resources()))
      .pipe(gulp.dest(path.join(config.path.build, 'scripts')));

    return merge(main, vendor);
  };
};
