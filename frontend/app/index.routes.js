(function () {
  'use strict';

  angular
    .module('angularGridApp')
    .config(routerConfig);

  function routerConfig($stateProvider, $urlRouterProvider, $locationProvider, routes) {
    $stateProvider
      .state(routes.app, {
        templateUrl: 'src/components/grid/grid.tpl.html',
        controller : 'GridCtrl as vm',
        url        : '/grid'
      });

    $urlRouterProvider.otherwise('/grid');

    $locationProvider.html5Mode(true);
  }
})();