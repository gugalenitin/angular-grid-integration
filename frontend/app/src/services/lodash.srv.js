(function () {
  'use strict';

  angular
    .module('angularGridApp')
    .factory('_', lodash);

  function lodash($window) {
    var _ = $window._;
    delete( $window._ );
    return ( _ );
  }
})();