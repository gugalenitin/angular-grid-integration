(function () {
  'use strict';

  angular.module('angularGridApp')
    .factory('urlTemplates', urlTemplates);

  function urlTemplates(_) {
    return {
      updateUser: _.template('users/<%= userId %>')
    };
  }
})();