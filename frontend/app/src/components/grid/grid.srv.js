(function () {
  'use strict';

  angular
    .module('angularGridApp')
    .constant('gridServiceConfig', {
      usersEndpoint: 'users'
    })
    .factory('gridService', gridService);

  function gridService(httpService, urlTemplates, gridServiceConfig) {
    return {
      getUsers  : getUsers,
      updateUser: updateUser
    };

    function getUsers() {
      return httpService.get(gridServiceConfig.usersEndpoint);
    }

    function updateUser(userEntity) {
      return httpService.put(urlTemplates.updateUser({ userId: userEntity.id }), userEntity);
    }
  }

})();
