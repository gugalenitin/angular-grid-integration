(function () {
  'use strict';

  angular
    .module('angularGridApp')
    .constant('gridConfig', {})
    .controller('GridCtrl', GridCtrl);

  function GridCtrl($scope, gridService) {

    var vm = this;

    vm.gridData = [
      {
        "firstName": "Cox",
        "lastName" : "Carney",
        "gender"   : "Male",
        "dob"      : "1st May",
        "field"    : "Engineering",
        "year"     : "2010",
        "interests": "Gaming",
        "age"      : "30"
      },
      {
        "firstName": "Cox",
        "lastName" : "Trump",
        "gender"   : "Male",
        "dob"      : "1st March",
        "field"    : "Medical",
        "year"     : "2011",
        "interests": "Music",
        "age"      : "27"
      },
      {
        "firstName": "Fox",
        "lastName" : "Carney",
        "gender"   : "Female",
        "dob"      : "1st March",
        "field"    : "Engineering",
        "year"     : "2010",
        "interests": "Music",
        "age"      : "27"
      },
      {
        "firstName": "Cox",
        "lastName" : "Carney",
        "gender"   : "Male",
        "dob"      : "1st May",
        "field"    : "Engineering",
        "year"     : "2011",
        "interests": "Gaming",
        "age"      : "30"
      },
      {
        "firstName": "Cox",
        "lastName" : "Trump",
        "gender"   : "Male",
        "dob"      : "1st March",
        "field"    : "Engineering",
        "year"     : "2011",
        "interests": "Music",
        "age"      : "27"
      },
      {
        "firstName": "Fox",
        "lastName" : "Trump",
        "gender"   : "Female",
        "dob"      : "1st March",
        "field"    : "Engineering",
        "year"     : "2011",
        "interests": "Gaming",
        "age"      : "27"
      },
      {
        "firstName": "Fox",
        "lastName" : "Carney",
        "gender"   : "Male",
        "dob"      : "1st May",
        "field"    : "Engineering",
        "year"     : "2010",
        "interests": "Music",
        "age"      : "30"
      },
      {
        "firstName": "Cox",
        "lastName" : "Carney",
        "gender"   : "Female",
        "dob"      : "1st March",
        "field"    : "Medical",
        "year"     : "2010",
        "interests": "Music",
        "age"      : "27"
      },
      {
        "firstName": "Cox",
        "lastName" : "Carney",
        "gender"   : "Female",
        "dob"      : "1st May",
        "field"    : "Medical",
        "year"     : "2011",
        "interests": "Gaming",
        "age"      : "27"
      },
      {
        "firstName": "Fox",
        "lastName" : "Trump",
        "gender"   : "Female",
        "dob"      : "1st March",
        "field"    : "Medical",
        "year"     : "2010",
        "interests": "Gaming",
        "age"      : "30"
      },
      {
        "firstName": "Cox",
        "lastName" : "Trump",
        "gender"   : "Female",
        "dob"      : "1st March",
        "field"    : "Medical",
        "year"     : "2011",
        "interests": "Music",
        "age"      : "27"
      },
      {
        "firstName": "Cox",
        "lastName" : "Trump",
        "gender"   : "Male",
        "dob"      : "1st May",
        "field"    : "Engineering",
        "year"     : "2011",
        "interests": "Gaming",
        "age"      : "30"
      },
      {
        "firstName": "Fox",
        "lastName" : "Trump",
        "gender"   : "Male",
        "dob"      : "1st May",
        "field"    : "Medical",
        "year"     : "2010",
        "interests": "Music",
        "age"      : "27"
      },
      {
        "firstName": "Cox",
        "lastName" : "Carney",
        "gender"   : "Male",
        "dob"      : "1st March",
        "field"    : "Engineering",
        "year"     : "2010",
        "interests": "Gaming",
        "age"      : "27"
      },
      {
        "firstName": "Cox",
        "lastName" : "Carney",
        "gender"   : "Male",
        "dob"      : "1st May",
        "field"    : "Medical",
        "year"     : "2010",
        "interests": "Music",
        "age"      : "30"
      },
      {
        "firstName": "Cox",
        "lastName" : "Carney",
        "gender"   : "Male",
        "dob"      : "1st March",
        "field"    : "Medical",
        "year"     : "2010",
        "interests": "Gaming",
        "age"      : "27"
      },
      {
        "firstName": "Fox",
        "lastName" : "Carney",
        "gender"   : "Male",
        "dob"      : "1st March",
        "field"    : "Medical",
        "year"     : "2010",
        "interests": "Gaming",
        "age"      : "27"
      },
      {
        "firstName": "Fox",
        "lastName" : "Trump",
        "gender"   : "Male",
        "dob"      : "1st May",
        "field"    : "Engineering",
        "year"     : "2010",
        "interests": "Music",
        "age"      : "30"
      }
    ];

    vm.gridOptions = {
      enableGridMenu       : true,
      enableCellEditOnFocus: true,
      columnDefs           : [
        { name: 'id', enableCellEdit: false },
        { name: 'firstName' },
        { name: 'lastName' },
        { name: 'email', enableCellEdit: false },
        { name: 'department' },
        { name: 'city' },
        { name: 'country' },
        { name: 'mobile' },
        { name: 'pinCode' },
        { name: 'age' },
        { name: 'interests' },
        { name: 'active', enableCellEdit: false }
      ]
    };

    init();

    function init() {
      gridService.getUsers()
        .then(function (response) {
            vm.gridOptions.data = response;
          },
          function () {
            vm.gridOptions.data = vm.gridData;
          });
    }

    vm.saveRow = function (rowEntity) {
      var promise = gridService.updateUser(rowEntity);
      vm.gridApi.rowEdit.setSavePromise(rowEntity, promise);
    };

    vm.gridOptions.onRegisterApi = function (gridApi) {
      //set gridApi on scope
      vm.gridApi = gridApi;
      gridApi.rowEdit.on.saveRow($scope, vm.saveRow);
    };
  }
})();
