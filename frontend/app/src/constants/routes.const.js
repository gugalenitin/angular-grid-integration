(function () {
  'use strict';

  angular.module('angularGridApp')
    .constant('routes', {
      app : 'app'
    });

})();