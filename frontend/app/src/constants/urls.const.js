(function () {
  'use strict';

  angular.module('angularGridApp')
    .constant('urls', {
      apiBase: '/api/'
    });

})();