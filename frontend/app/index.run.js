(function () {
  'use strict';

  angular
    .module('angularGridApp')
    .run(appRun);

  function appRun($urlRouter) {
    init();

    function init() {
      $urlRouter.sync();
      $urlRouter.listen();
    }
  }
})();