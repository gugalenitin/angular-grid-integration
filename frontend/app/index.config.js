(function () {
  'use strict';

  angular
    .module('angularGridApp')
    .config(config);

  function config($translateProvider, configurationProvider) {
    configurationProvider.startUp();
    $translateProvider.useSanitizeValueStrategy(null);
  }

})();
