(function () {
  'use strict';

  angular
    .module('angularGridApp', [
      'ui.router',
      'ui.bootstrap',
      'pascalprecht.translate',
      'ngAnimate',
      'ngCookies',
      'toastr',
      'ui.grid',
      'ui.grid.edit',
      'ui.grid.rowEdit',
      'ui.grid.cellNav',
      'ui.grid.resizeColumns',
      'ui.grid.exporter',
      'ui.grid.grouping',
      'ui.grid.infiniteScroll',
      'ui.grid.moveColumns'
    ]);
})();