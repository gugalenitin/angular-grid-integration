package com.myappdirect.myapi.api;

import com.myappdirect.myapi.dto.UserDTO;
import com.myappdirect.myapi.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<UserDTO> list() {
        return userService.getActiveUsers();
    }

    @RequestMapping(value = "/{userId}", method = RequestMethod.PUT)
    public UserDTO update(@RequestBody UserDTO user) {
        return userService.save(user);
    }

}