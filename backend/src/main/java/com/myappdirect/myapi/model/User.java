package com.myappdirect.myapi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.persistence.criteria.Predicate;

@Entity
@Getter
@Setter
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String department;

    private String city;

    private String country;

    private String mobile;

    private long pinCode;

    private int age;

    private String interests;

    private Boolean active;
}
