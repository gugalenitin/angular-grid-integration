package com.myappdirect.myapi.service;

import com.myappdirect.myapi.dto.UserDTO;
import com.myappdirect.myapi.model.User;

import java.util.List;

public interface UserService {

    List<UserDTO> getActiveUsers();

    UserDTO save(UserDTO userDTO);

}
