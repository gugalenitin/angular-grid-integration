package com.myappdirect.myapi.service.impl;

import com.myappdirect.myapi.dto.UserDTO;
import com.myappdirect.myapi.model.User;
import com.myappdirect.myapi.repository.UserRepository;
import com.myappdirect.myapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDTO> getActiveUsers() {
        return userRepository.findByActive(true)
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO save(UserDTO userDTO) {
        return convertToDTO(userRepository.save(convertToModel(userDTO)));
    }

    private UserDTO convertToDTO(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        userDTO.setEmail(user.getEmail());
        userDTO.setDepartment(user.getDepartment());
        userDTO.setCity(user.getCity());
        userDTO.setCountry(user.getCountry());
        userDTO.setMobile(user.getMobile());
        userDTO.setPinCode(user.getPinCode());
        userDTO.setAge(user.getAge());
        userDTO.setInterests(user.getInterests());
        userDTO.setActive(user.getActive());
        return userDTO;
    }

    private User convertToModel(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setDepartment(userDTO.getDepartment());
        user.setCity(userDTO.getCity());
        user.setCountry(userDTO.getCountry());
        user.setMobile(userDTO.getMobile());
        user.setPinCode(userDTO.getPinCode());
        user.setAge(userDTO.getAge());
        user.setInterests(userDTO.getInterests());
        user.setActive(userDTO.getActive());
        return user;
    }

}
