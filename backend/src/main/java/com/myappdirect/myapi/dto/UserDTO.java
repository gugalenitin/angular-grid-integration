package com.myappdirect.myapi.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO {
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String department;

    private String city;

    private String country;

    private String mobile;

    private long pinCode;

    private int age;

    private String interests;

    private Boolean active;
}
